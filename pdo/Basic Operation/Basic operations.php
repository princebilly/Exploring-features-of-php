<?php 
	define('DB_HOST', 'localhost');
	define('DB_USER', 'root');
	define('DB_KEY', '123456');
	
	$database= 'test';
	$dsn='mysql:host='.DB_HOST.';dbname='.$database;
	$pdo= new PDO($dsn,DB_USER,DB_KEY);
	
	/*=====================================
	=            SHOW ALL ROWS            =
	=====================================*/
	/*
	$sql= "SELECT * FROM posts";
	$stmt= $pdo->prepare($sql);
	$stmt->execute([]);
	$posts= $stmt->fetchAll(PDO::FETCH_OBJ);
	foreach ($posts as $post) {
		foreach ($post as $key => $value) {
			echo $key.': '.$value.'<br>';
		}
		echo '<br>';
	}
	/*=====  End of SHOW ALL ROWS  ======*/

	/*==========================================
	=            SHOW SPECIFIC ROWS            =
	==========================================*/
	/*
	$stmt = $pdo->prepare("SELECT * FROM posts WHERE author = ?");
	//$stmt->execute([]);
	$stmt->execute(['Prince']);
	$posts= $stmt->fetchAll(PDO::FETCH_OBJ);
	foreach ($posts as $post) {
		foreach ($post as $key => $value) {
			echo $key.': '.$value.'<br>';
		}
		echo '<br>';
	}
	/*=====  End of SHOW SPECIFIC ROWS  ======*/
	

	/*======================================
	=            SEARCH IN ROWS            =
	======================================*/
	/*	
	$subject ="%post 3%";
	$stmt= $pdo->prepare("SELECT * FROM posts WHERE body LIKE ? ");
	$stmt->execute([$subject]);
	$posts= $stmt->fetchAll(PDO::FETCH_OBJ);

	foreach ($posts as $post) {
		foreach ($post as $key => $value) {
			echo $key.": ".$value."<br>";
		}
		echo "<br>";
	}
	/*=====  End of SEARCH IN ROWS  ======*/
	

	/*===================================
	=            INSERT ROWS            =
	===================================*/
	/*
	$stmt = $pdo->prepare("INSERT INTO posts(title,author,body) VALUES(?,?,?);");
	$stmt->execute(['Postx','PrinceBilly','This is post']);	
	/*=====  End of INSERT ROWS  ======*/
	
	/*===================================
	=            DELETE ROWS            =
	===================================*/
	/*
	$stmt = $pdo->prepare("
						   DELETE FROM `posts` WHERE `posts`.`id` = 6;
						   DELETE FROM `posts` WHERE `posts`.`id` = 7;
						   DELETE FROM `posts` WHERE `posts`.`id` = 8;
					    ");
	$stmt->execute();
	/*=====  End of DELETE ROWS  ======*/
	 ?>