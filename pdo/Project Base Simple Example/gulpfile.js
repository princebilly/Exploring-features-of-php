var gulp = require('gulp');
var imagemin = require('gulp-imagemin');
var uglify= require('gulp-uglify');
var sass= require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var notify= require('gulp-notify');

 gulp.task('massage',function() {
    return console.log('Gulp is running');
	}); 
 
gulp.task('imageMin', function(){
    gulp.src('src/img/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/img'))
        .pipe(notify({ message: 'Image optimized' }));
    });

gulp.task('copyphp', function() {
    gulp.src('src/**/*.php')
     	.pipe(gulp.dest('dist'))
     	.pipe(notify({ message: 'php changed' }));
});
gulp.task('minify', function() {
    gulp.src('src/js/**/*.js')
      .pipe(uglify())
      .pipe(gulp.dest('dist/js')).pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
      .pipe(notify({ message: 'js modified' }));
});

gulp.task('sass', function() {
  return gulp.src('src/scss/style.scss')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(autoprefixer({browsers: ['last 2 versions'],cascade: false}))
    .pipe(gulp.dest('dist/css'))
    .pipe(notify({ message: 'sass compiled' }));
});

/*
gulp.task('sass',function(){
	gulp.src()
		.pipe(sass({}).on('error',sass.logError))
		.pipe(gulp.dest());
});
//*/

gulp.task('default',['massage','imageMin','copyphp','sass','minify']);

gulp.task('watch',function(){
	gulp.watch('src/scss/*.scss',['sass']);
	gulp.watch('src/img/**/*', ['imagemin']);
	gulp.watch('src/**/*.php', ['copyphp']);
	gulp.watch('src/js/**/*.js', ['minify']);
});