	<section id="jumbotorn" class="container">
			<div class="jumbotron">
			<h5 class="display-4"><?php echo $title ?></h5>
			<p class="lead">ur laboriosam tempora similique est eligendi quibusdam rerum eum nesciunt, ipsam soluta minus, voluptas? </p>
			<hr class="m-y-md">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores debitis non, consequuntur obcaecati hic eius aperiam cum atque rem ipsum.</p>
			<p class="lead">
				<a class="btn btn-primary btn-lg" href="#" role="button">Sign up today</a>
			</p>
		</div>
	</section>

	<section id="jobs" class='container'>
		<?php foreach ($jobs as $job): ?>
			<div class="row mb-5">
				<div class="col-md-10">
					<h5><?php echo $job->job_title ?></h5>
					<p><?php echo $job->description ?></p>
				</div>
				<div class="col-md-2">
					<a class="btn btn-primary" href="#" role="button">View</a>
				</div>
			</div>
		<?php endforeach ?>
	</section>
