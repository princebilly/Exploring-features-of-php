<?php 
	class Database{
		private $host= DB_HOST;
		private $username= DB_USER;
		private $key= DB_KEY;
		private $database=DB_NAME;

		private $dbh;
		private $error;
		private $stmt;

		public function __construct(){
			$dsn = "mysql:host=".$this->host.";dbname=".DB_NAME;
			$option= array(
				PDO::ATTR_PERSISTENT => true,
				PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
			);
			try{
				$this->dbh= new PDO($dsn,$this->username,$this->key,$option);
			}catch(PDOException $e){
				$this->$e->getMessage();
			}
		}

		public function query($sql){
			$this->stmt=$this->dbh->prepare($sql);
		} 
		public function execute($array=[]){
			return $this->$stmt->execute($array);
		}
		public function getAllValue($array=[]){
			$this->stmt->execute($array);
			return $this->stmt->fetchALL(PDO::FETCH_OBJ);
		}
		public function getValue($array=[]){
			$this->stmt->execute($array);
			return $this->$stmt->fetch(PDO::FETCH_OBJ);
		}
	}
 ?>