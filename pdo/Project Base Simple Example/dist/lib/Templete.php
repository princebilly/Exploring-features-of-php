<?php 
	class Templete{
		protected $path;
		protected $varriables= array();
		
		//set templete directory
		public function __construct($path){
			$this->path=$path;
		}

		//set templete varriables;
		public function __set($key,$val){
			$this->varriables[$key]=$val; 
		}

		//get templete varriables
		public function __get($key){
			return $this->varriables[$key];
		}

		//print templete
		public function __toString(){
			extract($this->varriables);
			ob_start();
			include 'templete/'.str_replace('\\', '/', $this->path).'.php';
			return ob_get_clean();
		}


	}
 ?>