<?php 
	class job{
		private $db;

		public function __construct(){
			$this->db= new Database;
		}

		public function getAllJobs(){
			$db->query("SELECT jobs.*, categories.name AS cname 
						FROM jobs 
						INNER JOIN categories
						ON jobs.category_id = categories.id 
						ORDER BY post_date DESC 
						");
			return $db->getAllValue();
		}
	}
 ?>