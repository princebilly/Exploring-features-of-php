<?php
//
// ──────────────────────────────────────────────────────────────────── I ──────────
//   :::::: O B J E C T   C L O N I N G : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────────────────────────
//
/*********************************************************************************
 * ASSIGNING A OBJECT TO ANOTHER OBJECT CREATES A REFERENCE OF SAME MEMORY WHERE *
 *             CLONING A OBJECT CREATES A GENUE COPY OF A OBJECT.                *
 *********************************************************************************/

 //
 // ──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────── II ──────────
 //   :::::: W H A T   H A P P E N S   W H E N   W E   A S S I G N   A   V A R R I A B L E ? : :  :   :    :     :        :          :
 // ──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
 //
    class myClass{
        public  $var="a";
        public $var2= "b";
    }
    $obj1= new myClass;
    $obj2=$obj1;
    $obj3=$obj2;
    $obj2->var='c';
    $obj3->var2='d';

    var_dump($obj1);echo '<br>';
    var_dump($obj2);echo '<br>';
    var_dump($obj3);echo '<br>';
    //changing  property of a any obj is changing all those obj

    ////////////////////////////////////////////////////////////////////////////////
    // object(myClass)#1 (2) { ["var"]=> string(1) "c" ["var2"]=> string(1) "d" } //
    // object(myClass)#1 (2) { ["var"]=> string(1) "c" ["var2"]=> string(1) "d" } //
    // object(myClass)#1 (2) { ["var"]=> string(1) "c" ["var2"]=> string(1) "d" } //
    ////////////////////////////////////////////////////////////////////////////////

    echo '<br>';
    echo '<br>';
    echo '<br>';

//
// ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────── II ──────────
//   :::::: W H A T   H A P P E N S   W H E N   W E   C L O N E   A   O B J   I N S T E A D   O G   A S S I G N I N G : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
//
    class anotherClass{
        public  $var="a";
        public $var2= "b";
    }
    $obj1= new anotherClass;
    $obj2=clone $obj1;
    $obj3=clone $obj2;
    $obj2->var='c';
    $obj3->var2='d';

    var_dump($obj1);echo '<br>';
    var_dump($obj2);echo '<br>';
    var_dump($obj3);echo '<br>';
    //changing an object does not affects other objects

      /////////////////////////////////////////////////////////////////////////////////////
     //object(anotherClass)#2 (2) { ["var"]=> string(1) "a" ["var2"]=> string(1) "b" }  //
    //object(anotherClass)#3 (2) { ["var"]=> string(1) "c" ["var2"]=> string(1) "b" }  //
   //object(anotherClass)#4 (2) { ["var"]=> string(1) "a" ["var2"]=> string(1) "d" }  //
  /////////////////////////////////////////////////////////////////////////////////////
?>