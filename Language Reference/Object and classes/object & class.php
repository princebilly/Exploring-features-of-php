<?php
//
// ──────────────────────────────────────────────────────────────────────────────────────────────────────── I ──────────
//   :::::: U S E   O F   S C O P E   R E S U L A T I O N   O P E R A T O R : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────────────────────────────────────────────────────────────
//
//
// ─── GETCLASS NAME ──────────────────────────────────────────────────────────────
//
/*********************************************************************
 * WE CAN USE A RESOLUSION OPERATOR(::) WITH class KEYWORD LIKE THIS *
 *                        'CLASS_NAM::class'                         *
 *                         TO GET CLASS NAME                         *
 *********************************************************************/
    class anyClass{
        public function __construct(){
            echo 'Class name is: '.anyClass::class.'<br>';
        }
    }

    $obj = new anyClass(); //prints class name
    #we can also access it like this as it is a static memeber:
    echo 'Class name is: '.anyClass::class.'<br>';   //prints class name

//
// ─── ACCESS STATIC MEMBER ───────────────────────────────────────────────────────
//
/***************************************************************************************
 * WE CAN ACCESS ANY STATIC MEMEBER OUTSIDE OF A CLASS USING SCOPE RESOLUSION OPERATOR *
 ***************************************************************************************/
    class myClass{
        public static $var="Hello world<br>";
        public static function stat_pub(){
            echo 'Called public static stat_pub()<br>';
        }
        private static function stat_pri(){
            echo 'Called private static stat_pri()<br>';
        }
    }
    echo myClass::$var;
    myClass::stat_pub();
    #myClass::stat_pri(); error
/*******************************************************
 * PRIVATE FUNCTION CANNOT BE CALLED OUTSIDE THE CLASS *
 *                EVEN IF IT IS STATIC                 *
 *******************************************************/
//
// ──────────────────────────────────────────────────────────────────────────────── II ──────────
//   :::::: U S E   O F   F I N A L   K E Y W O R D : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────────────────────────────────────
//
/************************************************
 * FINAL KEYWORDS CAN ONLY BE USED WITH METHODS *
 ************************************************/
/***********************************************************************
 * A METHOD WITH FINAL KEYWORD CANNOT BE OVERWRITTEN  BY DERIVED CLASS *
 ***********************************************************************/
    class base{
        final public function finalized(){
            echo 'Cannot be overwritten<br>';
        }
    }
    /*
    class mew extends base{
        public function finalized(){ //gives error
            echo 'gives error';
        }
    }
    //*/

 //
 // ────────────────────────────────────────────────────────────────────────────────────────── III─────────
 //   :::::: W H A T   P H P   D O E S   N O T   S U P P O R T : :  :   :    :     :        :          :
 // ────────────────────────────────────────────────────────────────────────────────────────────────────
 //

/*********************************************
 * PHP DOES NOT SUPPORT MULTIPLE INHERITANCE *
 *********************************************/
 //but we can use traits instead of multiple inheritance

/******************************************************************************
 * WE CAN NOT DEFINE A MEMBER FUNCTION OUTSIDE A CLASS USING SCOPE RESULATION *
 ******************************************************************************/
   
?>