<?php
//
// ──────────────────────────────────────────────────────────────────── I ──────────
//   :::::: W H A T   I S   T R A I T S : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────────────────────────
//
/*************************************************************
 *       AS WE ALL KNOW PHP DOES NOT SUPPORT MULTIPLE        *
 * INHERITANCE SO HERE TRAITS ALLOWS US TO DO THE SAME THING *
 *************************************************************/
//
// ────────────────────────────────────────────────────── II ──────────
//   :::::: E X A M P L E : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────
//

    class base{
        public function baseFunc(){
            echo 'inherited from base<br>';
        }
    }
    trait trait1{                   //trait can be define using trait keyword
        public function traitFunc1(){
            echo 'inherited from trait1<br>';
        }
        private function traitFunc1_2(){
            echo 'inherited from trait1_2<br>';
        }

    }

    trait trait2{
        public function traitFunc2(){
            echo 'inherited from trait2<br>';
        }
    }

    trait trait3{
        public function traitFunc3(){
            echo 'inherited from trait3<br>';
        }
    }

    class derived extends base{
        use trait1, trait2, trait3;  //we can add multiple trait using comma(,)
    }//use keyword to add trait

    $OBJ=new derived;
    $OBJ->baseFunc();
    $OBJ->traitFunc1();
    $OBJ->traitFunc2();
    $OBJ->traitFunc3();

    //////////////////////////
    // ouput:               //
    // inherited from base  //
    // inherited from trait1//
    // inherited from trait2//
    // inherited from trait3//
    //////////////////////////
//
// ────────────────────────────────────────────────────────────────────────────────────────────────────────────── III ──────────
//   :::::: M U L T I P L E   T R A I T   C A L L   U S I N G   O N E   T R A I T : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
//

    trait multitrait{
        use trait1, trait2, trait3;
    }
    class newClass{
        use multitrait;     
    }
    $newObj= new newClass();
    $newObj->traitFunc1();
    $newObj->traitFunc2();
    $newObj->traitFunc3();
    //////////////////////////
    // ouput:               //
    // inherited from trait1//
    // inherited from trait2//
    // inherited from trait3//
    //////////////////////////

//
// ──────────────────────────────────────────────────────────────────────────────────────────── IV ──────────
//   :::::: C H A N G E   T R A I T   F U N C T I O N   N A M E : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────────────────────────────────────────────────
//

/******************************************************************************
 * WE CAN CHANGE THE NAME AND VISIBLITY OF TRAIT'S FUNCTIONS USING as KEYWORD *
 ******************************************************************************/

    class dummyClass{
        use trait1{
            traitFunc1 as myNewFunc;
            traitFunc1_2 as public myNewFunc1;
         } //no cemecolon here
    }

    $obj= new dummyClass;
    $obj->myNewFunc();
    $obj->myNewFunc1();

    ////////////////////////////
    // Output:                //
    // inherited from trait1  //
    // inherited from trait1_2//
    ////////////////////////////
?>