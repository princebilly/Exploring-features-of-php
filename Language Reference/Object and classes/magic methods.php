<?php
//
// ────────────────────────────────────────────────────────────────────────────────── I ──────────
//   :::::: W H A T   I S   M A G I C   M E T H O D S : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────────────────────────────────
//
/********************************************************************************
 *  THE FUNCTION NAMES __construct(), __destruct(), __call(), __callstatic(),   *
 * __get(), __set(), __isset(), __unset(), __sleep(), __wakeup(), __tostring(), *
 *  __invoke(), __set_state(), __clone() and __debuginfo() ARE MAGICAL IN PHP   *
 *  CLASSES. YOU CANNOT HAVE FUNCTIONS WITH THESE NAMES IN ANY OF YOUR CLASSES  *
 *        UNLESS YOU WANT THE MAGIC FUNCTIONALITY ASSOCIATED WITH THEM.         *
 ********************************************************************************/

 //
 // ────────────────────────────────────────────────────────────────── III ──────────
 //   :::::: M A G I C   M E T H O D S : :  :   :    :     :        :          :
 // ────────────────────────────────────────────────────────────────────────────
 //

//
// ─── _sleep() and _wakeup() ────────────────────────────────────────────────
//
/*********************************
 * public array __sleep ( void ) *
 *    void __wakeup ( void )     *
 *********************************/

/*************************************************************************************
 * USING _sleep() FUNCTION WE CAN MODIFY THE WORKING PROCESS OF serialize() FUNCTION *
 *************************************************************************************/
/**********************************************************************
 * USING _wakeup() WE CAN MODIFY THE WORKING PROCESS OF unserialize() *
 **********************************************************************/
/*******************************************
 * BOTH OF THEM CAN ONLY BE USED AS PUBLIC *
 *******************************************/

 class Class1{
    public $var;
    private $var_p;
    public function changeval(){
        $this->var = 'Something';
        $this->var_p ='Something untold';
    }
    public function __sleep(){
        return ['var'];     
    }
    public function  __wakeup(){;
        $this->var_p='Who cares';
    }
 }

  $v1= new Class1;
  $v1->changeval();
  var_dump($v1); echo '<br>';
  $v2= unserialize(serialize($v1));
  var_dump($v2); echo '<br>';

  output:
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // object(Class1)#1 (2) { ["var"]=> string(9) "Something" ["var_p":"Class1":private]=> string(16) "Something untold" }//
  // object(Class1)#2 (2) { ["var"]=> string(9) "Something" ["var_p":"Class1":private]=> string(9) "Who cares" }        //
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/***************************************************************************
 * __wakeup() IS GENARALLY USED TO RECONNECT THE SERVER CONECTION THAT MAY *
 *                  HAVE BEEN LOST DURING SERIALIZATTION                   *
 ***************************************************************************/



 //
 // ─── __tostring() ───────────────────────────────────────────────────────────────────
 // 
/*************************************
 * public string __tostring ( void ) *
 *************************************/
/*********************************************************************************
 * THE __TOSTRING() METHOD ALLOWS A CLASS TO DECIDE HOW IT WILL REACT WHEN IT IS *
 *                            TREATED LIKE A STRING.                             *
 *********************************************************************************/
/****************************************
 * TO STRING CAN ONLY BE USED AS PUBLIC *
 ****************************************/
    class printme{
        public function __tostring(){
            return  'Thank you for printing me ^_^<br>';
        }
    }
    $myClass= new printme();
    echo $myClass;
    //////////////////////////////////
    // Thank you for printing me ^_^//
    //////////////////////////////////


//
// ─── __invoke() ─────────────────────────────────────────────────────────────────────
//
/******************************
 * mixed __invoke ([ $... ] ) *
 ******************************/
/****************************************************************************************
 * THE __invoke() METHOD IS CALLED WHEN A SCRIPT TRIES TO CALL AN OBJECT AS A FUNCTION. *
 ****************************************************************************************/
/*******************************************
 * INVOKE ALLOWS PUBLIC, PRIVATE, PROTCTED *
 *******************************************/
 
    class hurray{
        public function __invoke(){
            echo 'WOW!!! you can use me like a function ^_^ <br>';
        }
    }
    $mew= new hurray;
    $mew();

//
// ─── __debugInfo() ──────────────────────────────────────────────────────────────────
//
/******************************
 * ARRAY __DEBUGINFO ( VOID ) *
 ******************************/
/*****************************************************************************************
 * THIS METHOD IS CALLED BY var_dump() WHEN DUMPING AN OBJECT TO GET THE PROPERTIES THAT *
 * SHOULD BE SHOWN. IF THE METHOD ISN'T DEFINED ON AN OBJECT, THEN ALL PUBLIC, PROTECTED *
 *                         AND PRIVATE PROPERTIES WILL BE SHOWN.                         *
 *****************************************************************************************/
class normalClass{
    public $var=100;
    private $var_p=200;
}
$mew= new normalClass;
var_dump($mew);
echo '<br>';
$mew= new class extends normalClass{
    public function __debugInfo(){
        return [
            'varTripple'=>$this->var*3
        ];
    } 
};
var_dump($mew);
echo '<br>';

//
// ─── __set_state() ──────────────────────────────────────────────────────────────────
//

/***************************************************
 * static object __set_state ( array $properties ) *
 ***************************************************/
/*********************************************************************
 * THIS STATIC METHOD IS CALLED FOR CLASSES EXPORTED BY VAR_EXPORT() *
 *********************************************************************/
    class setStateClass{
       public $var;
       private $var_p;
       public function setVar(){
           $this->var="-_-";
           $this->var_p='^_^';
       }
    }
    $mew= new setStateClass;
    $mew->setVar();
    var_export($mew); 
    echo '<br>';
    $mew= new class extends setStateClass{
        public static function __set_state($myArray){
            $myVar= new setStateClass;
            $myVar->var=$myArray['var'];
            $myVar->var_p='I dont want to show you private data sorry';
            return $myVar;
          }
    };
    $mew->setVar();
    var_export($mew);
    echo '<br>';
//
// ─── __clone() ────────────────────────────────────────────────────────────────────────
//
/*************************
 * void __clone ( void ) *
 *************************/
/****************************************************************
 * __clone() FUNCTION CUSTOMIZE THE BEHAVIOUR OF OBJECT CLONING *
 ****************************************************************/
    class myClass{
        public  $var=2;
        public $var2= "b";
        public function __clone(){
            $this->var= $this->var*2;
            $this->var2='d';
        }
    }
    $obj1= new myClass;
    $obj2=clone $obj1;
    

    var_dump($obj1);echo '<br>';
    var_dump($obj2);echo '<br>';
    //////////////////////////////////////////////////////////////////////////
    // Output:                                                              //
    // object(myClass)#5 (2) { ["var"]=> int(2) ["var2"]=> string(1) "b" }  //
    // object(myClass)#6 (2) { ["var"]=> int(4) ["var2"]=> string(1) "d" }  //
    //////////////////////////////////////////////////////////////////////////
//
// ─── __call() and __callStatic() ────────────────────────────────────────────────────────────────────────
//        
   
/************************************************************************
 *       PUBLIC MIXED __call ( string $name , array $arguments )        *
 * PUBLIC STATIC MIXED __callstatic ( string $name , array $arguments ) *
 ************************************************************************/
/***************************************************************************************
 *   __call() IS TRIGGERED WHEN INVOKING INACCESSIBLE METHODS IN AN OBJECT CONTEXT.    *
 * __callstatic() IS TRIGGERED WHEN INVOKING INACCESSIBLE METHODS IN A STATIC CONTEXT. *
 ***************************************************************************************/
/*****************************************************************************************
 * THE $NAME ARGUMENT IS THE NAME OF THE METHOD BEING CALLED. THE $ARGUMENTS ARGUMENT IS *
 *      AN ENUMERATED ARRAY CONTAINING THE PARAMETERS PASSED TO THE $NAME'ED METHOD      *
 *****************************************************************************************/
    class callMe{
        public  $var=2;
        public $var2= "b";
        private function callme_p($class){
            echo 'INACCESSIBLE METHODS';
        }

        protected function callme_pro($class){
            echo 'INACCESSIBLE METHODS ';
        }
        private static function callme_ps($class){
            echo 'INACCESSIBLE METHODS';
        }

        protected static function callme_pros($class){
            echo 'INACCESSIBLE METHODS ';
        }
        function __call($name,$arguments)
        {
            echo $name.'I am INACCESSIBLE METHODS <br>';
        }
        function __callStatic($name,$arguments)
        {
            echo $name.'I am STATIC INACCESSIBLE METHODS <br>';
        }
    }
    $obj= new callMe;
    $obj->callme_p();
    $obj->callme_pro();
    $obj->callme_ps();
    $obj->callme_pros();
 //public static mixed __callStatic ( string $name , array $arguments )

 //
 // ─── __set() and __get() ────────────────────────────────────────────────────────────────
 //

/*****************************************************
 * public void __set ( string $name , mixed $value ) *
 *        public mixed __get ( string $name )        *
 *****************************************************/
/**********************************************************************
 *    __set() IS RUN WHEN WRITING DATA TO INACCESSIBLE PROPERTIES.    *
 * __get() IS UTILIZED FOR READING DATA FROM INACCESSIBLE PROPERTIES. *
 **********************************************************************/

    class accessMe{
        private  $var;
        public function __set($name, $value){
            echo 'this is weird function, isn\'t it?<br>';
        }  
        public function __get($name){
            return 'this is weird function too, isn\'t it?<br>';
        } 
    }
    $obj= new accessMe;
    $obj->var= '3';
    echo $obj->var;

     
 //
 // ───__isset() and __unset() ───────────────────────────────────────────────────────────────────────────
 //

/****************************************
 * public bool __isset ( string $name ) *
 * public void __unset ( string $name ) *
 ****************************************/
/************************************************************************************
 * __isset() IS TRIGGERED BY CALLING ISSET() OR EMPTY() ON INACCESSIBLE PROPERTIES. *
 *      __unset() IS INVOKED WHEN UNSET() IS USED ON INACCESSIBLE PROPERTIES.       *
 ************************************************************************************/

    class amiset{
        private  $var;
        public function __isset($name){
            echo 'this is weird function, isn\'t it?';
        }  
        public function __unset($name){
            echo 'this is weird function too, isn\'t it?<br>';
        } 
    }
    $obj= new amiset;
    var_dump(isset($obj->var));
    echo '<br>';
    unset($obj->var);
 ?>



