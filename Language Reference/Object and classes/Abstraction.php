<?php
//
// ──────────────────────────────────────────────────────────────────────────────────────────────────── I ──────────
//   :::::: P H P   S U P P O R T S   C L A S S   A B S T R A C T I O N : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────────────────────────────────────────────────────────
//
/****************************************************************************************
 * ABSTRACT CLASS IS NOT GENARALLY USED TO DECLARE OBJECT. A ABSTACT CLASS IS USED AS A *
 *                    BASE CLASS TO BE INHERITED BY OTHER CLASS                         *
 ****************************************************************************************/
 //
 // ────────────────────────────────────────────────────────────────────────────────────── II ──────────
 //   :::::: R U L E   F O R   A B S T R A C T   C L A S S : :  :   :    :     :        :          :
 // ────────────────────────────────────────────────────────────────────────────────────────────────
 //

/****************************************************************************
 * ABSTRACT CLASS CAN CONTAIN PROPERTIES, ABSTRACT METHOD AND NORMAL METHOD *
 ****************************************************************************/

/********************************************************************************************
 * WE CAN MAKE MANDATORY FUNCTIONS TO BE DEFINED IN INHERITED CLASS USING ABSTRACT KEYWORD. *
 * A CLASS WITH ABSTRACT KEYWORD NEED NOT TO BE DEFINED IN ABSTRACT CLASS.                  * 
 ********************************************************************************************/

/*****************************************************************************************************
 *  IF THE ABSTRACT METHOD IS DEFINED AS PROTECTED, THE FUNCTION IMPLEMENTATION MUST BE DEFINED AS   *
 * EITHER PROTECTED OR PUBLIC, BUT NOT PRIVATE. FURTHERMORE THE SIGNATURES OF THE METHODS MUST MATCH *
 *****************************************************************************************************/

/***************************************************************
 * IN A ABSTRACT CLASS A ABSTRACT FUNCTION CANNOT HAVE A BODY. *
 ***************************************************************/

//
// ────────────────────────────────────────────────────── III ──────────
//   :::::: E X A M P L E : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────
//

    abstract class baseCLass{
        public $var='Hello world<br>';
        public $var2;
        #$var='this is a base class'; 
        //give error as  we cannot declare or define any properties/variable in abstract class
    // ─────────────────────────────────────────────────────────────────
        abstract public function foo($name); //a abstract function cannot contain body
    // ─────────────────────────────────────────────────────────────────
        public static function zoo(){
            echo 'this is a static function<br>';
        }    
        public function boo(){
            echo $this->var;
        }
    }   

    class derivedClass extends baseClass{
        public function foo($name){
            echo $name;
        }
    }

    $obj = new derivedClass();
    $obj->foo('Hello billy<br>');
    $obj->boo();
    ?>