<?php
//
// ────────────────────────────────────────────────────────────────────── I ──────────
//   :::::: C L A S S   I N T E R F A C E : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────────────────────
//
/*****************************************************************************************
 * OBJECT INTERFACES ALLOW YOU TO CREATE CODE WHICH SPECIFIES WHICH METHODS A CLASS MUST *
 *        IMPLEMENT, WITHOUT HAVING TO DEFINE HOW THESE METHODS ARE IMPLEMENTED.         *
 *****************************************************************************************/

//
// ────────────────────────────────────────────────────────────────────────────── II ──────────
//   :::::: R U L E S   F O R   I N T E R F A C E : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────────────────────────────
//
/***********************************************************
 * A INTERFACE CAN ONLY HAVE PUBLIC FUNCTION AND CONSTANTS *
 ***********************************************************/
    interface foo{ //interface keyword to define a class interface
        const a= 'it is a constant';
        public static function hello($var1, $var2);
        //private function boo(); error 
    }
    class myClass implements foo{
        public static function hello($var1, $var2){
            echo 'does nothing';
        }
    }
    echo foo::a;
    echo myClass::a;
?>