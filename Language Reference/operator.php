<?php
//
// ──────────────────────────────────────────────────────────────────────────────────── I ──────────
//   :::::: E R R O R   C O N T R O L   O P E R A T O R : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────────────────────────────────────────
//

    /*
    if(isset($_GET[action])){
        $var_er=$_GET['action'];
        echo 'No error<br>var:'.$var.'<br>';
    }else{
        die("Failed to GET varraible<br>");
    }
    */
/****************************************************************************
 * THE FOLLOWING CODE CAN BE DONE SO EASILY BY USING ERROR CONTROL OPERATOR *
 ****************************************************************************/
 /***************************
 * @ PREVENT SHOWING ERROR *
 ***************************/  
    //$var_er1 = @$_GET['action'] or die("Failed to GET varraible<br>");
/**************************************
 * HERE @ IS A ERROR CONTROL OPERATOR *
 **************************************/
    ////////////////////////////
    // output:                //
    // Failed to GET varraible//
    ////////////////////////////

//
// ──────────────────────────────────────────────────────────────────────────── II ──────────
//   :::::: B A C K T I C K S   O P E R A T O R : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────────────────────────────────
//

    $terminal = `lsb_release -a`; // $terminal will return the outputs of this shell command
    echo "<pre>$terminal</pre>"; //when we print this, the command shows its output 
    /////////////////////////////////////
    // Output:                         //
    // Distributor ID:	Ubuntu         //
    // Description:	Ubuntu 18.04.1 LTS //
    // Release:	18.04                  //
    // Codename:	bionic             //
    /////////////////////////////////////

/*******************************************************
 * BACKTICKS OPERATOR ARE USE TO EXCUTE SHELL COMMANDS *
 ******************************************************/
//
// ────────────────────────────────────────────────────────────────────── END ─────
//

    
?>