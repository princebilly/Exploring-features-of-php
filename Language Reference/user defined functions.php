<?php
//
// ────────────────────────────────────────────────────────────────────────────────── I ──────────
//   :::::: W H A T   D O E S   P H P   S U P P O R T : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────────────────────────────────
//
//
// ─── REFERENCE VARIABLE ─────────────────────────────────────────────────────────
//
/************************************
 * PHP SUPPORTS REFERENCE VARRAIBLE *
 ************************************/
    function main(){
        $var='Hello';
        echo $var.'<br>'; //outputs: Hello
        change($var);
        echo $var.'<br>'; //outputs: Hello world
        
    }
    function change(&$ref){
        $ref .= ' world';
    }
    main();
//
// ─── DEFAULT ARGUMENTS ──────────────────────────────────────────────────────────
//
/**********************************************
 * PHP SUPPORTS DEFAULT ARGUMENTS IN FUNCTION *
 **********************************************/
    function give_me_some_drinks($drink='wine'){
        echo 'Here is your '.$drink.' sir<br>';
    }
    give_me_some_drinks();
    give_me_some_drinks('milk');

    //////////////////////////
    // Here is your wine sir//
    // Here is your milk sir//
    //////////////////////////
//
// ─── NESTED FUNCTION ────────────────────────────────────────────────────────────
//
/********************************
 * PHP SUPPORTS NESTED FUNCTION *
 ********************************/
    function foo(){
        echo 'foo() is called'.'<br>';
        function too(){
            echo 'foo()->too() is called'.'<br>';
        }
        function boo(){
            echo 'foo()->boo() is called'.'<br>';
            function xoo(){
                echo 'foo()->boo()->xoo() is called'.'<br>';
            }
            function moo(){
                echo 'foo()->boo()->moo() is called'.'<br>';
            }
        }
        function zoo(){
            echo 'foo()->zoo() is called'.'<br>';
            function xoo(){     //same name as used in boo()->xoo();
                echo 'zoo()->xoo() is called'.'<br>';
            }
        #we can use same name for nested function more than once 
        #but we can not call more than one of the parent function
        }
    }

/****************************************************************
 * TO CALL A INNER FUNCTION YOU MUST CALL OUTER FUNCTIONS First *
 ****************************************************************/
    #xoo();//error: we have to declare foo first as it is nested in foo()
 
    function test1(){
        echo '<b>test1:</b><br>';
        foo(); //call foo()
        too();
        boo();
        too(); // we can can a function twice
        moo(); // moo() can be called as we have already called boo()
        xoo(); // xoo() can be called as we have already called boo()
        #zoo(); gives redeclaration error
        //we cannont call zoo() because we have already called boo() and both of them have same named nested function xoo()
    }

    function test2(){
        echo '<b>test2:</b><br>';
        foo(); //call foo()
        too();
        #moo(); 
        //we can not call moo() as the parent function boo() is not yet called
        zoo(); 
        xoo();
        #boo(); gives redeclaration error
        //we cannont call boo() because we have already called zoo() and both of them have same named nested function xoo()

    }
    //test1();
    test2(); 
    // we can not call both test1() and test2() at a time it will also cause redeclaration error
    
    ////////////////////////////////////////////////////////////////////
    // ////////////////////////////////  //////////////////////////// //
    // Output for test1()           //  // Output for test2()     //  //
    // test1:                       //  // test2:                 //  //
    // foo() is called              //  // foo() is called        //  //
    // foo()->too() is called       //  // foo()->too() is called //  //
    // foo()->boo() is called       //  // foo()->zoo() is called //  //
    // foo()->too() is called       //  // zoo()->xoo() is called //  //
    // foo()->boo()->moo() is called//  ////////////////////////////  //
    // foo()->boo()->xoo() is called//                                //
    // ////////////////////////////////                               //
    ////////////////////////////////////////////////////////////////////

//
// ────────────────────────────────────────────────────────────────────────────────────────── II ──────────
//   :::::: W H A T   P H P   D O E S   N O T   S U P P O R T : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────────────────────────────────────────
//  
/*********************************************
 * PHP DOES NOT SUPPORT FUNCTION OVERLOADING *
 *********************************************/
    //////////////////////////////////////////////////////////////////////////
    // function write(){                                                    //
    //     echo 'Hello world'.'<br>';                                       //
    // }                                                                    //
    // function write($text){ Gives error                                   //
    //     echo $text.'<br>';                                               //
    // }                                                                    //
    // function write($text1, $text2){     Gives error                      //
    //     echo $text1.'<br>'.$text2.'<br>';                                //
    // }                                                                    //
    // write();                                                             //
    // write('Prince Billy Graham');                                        //
    // write('Shamsul Tazour Rafi','Samin Shoumik');                        //
    // //////////////////////////////////////////////////////////////////// //
    // output:                                                          //  //
    // Fatal error: Cannot redeclare write() (previously                //  //
    // declared in /home/princebilly/lampstack/apache2/htdocs/          //  //
    // web/phpexplore/functions.php:38) in /home/princebilly/           //  //
    // lampstack/apache2/htdocs/web/phpexplore/functions.php on line 42 //  //
    // //////////////////////////////////////////////////////////////////// //
    //////////////////////////////////////////////////////////////////////////
 ?>
