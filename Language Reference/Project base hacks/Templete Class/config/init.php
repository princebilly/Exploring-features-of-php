<?php
require_once 'config/config.php';

spl_autoload_register(function($className){
    include 'lib/'.str_replace('\\','/',$className).'.php';
})

?>