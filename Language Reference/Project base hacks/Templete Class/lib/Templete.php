<?php
    class Templete{
        protected $path;
        protected $vars = array();
        
        #contrctor
        //get the dir name
        public function __construct($dir){
            $this->path=$dir;
        }

        //gets the key and returns it from the vars array
        public function __get($key){
            //*
            if(isset($this->vars[$key])){
            //*/
                return $this->vars[$key];
            //*
            }else{
                return 'value not defined';
            }//*/
        }

        //gets a key and value and set it to vars array
        public function __set($key, $value){
            $this->vars[$key]=$value;
        }

        //print the page
        public function __tostring(){
            extract($this->vars);
            ob_start();
            include $this->path;
            return ob_get_clean();
         }
    }
?>

