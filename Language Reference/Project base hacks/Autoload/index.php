<?php 
//
// ──────────────────────────────────────────────────────────────────────── I ──────────
//   :::::: W H A T   I S   A U T O L O A D : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────────────────────────────
//

/**************************************************
 *         AUTOLOADER FUNCTIONS ARE THIS:         *
 *       _autoload spl_autoload_register()        *
 * A AUTOLOADER AUTOMATICALLY RUN A FUNCTION WHEN *
 *              A CLASS IS INITIATED              *
 **************************************************/

/******************************************************
 * GENARALLY WE USE TO AUTOLOAD TO INCLUDE OR REQUIRE *
 *         A CLASS FILE  WHEN IT IS INITIATED         *
 ******************************************************/

//
// ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────── II ──────────
//   :::::: U S E   S P L   A U T O L O A D   R E G I S T E R   I N S T E A D   O F   A U T O L O A D : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
//

/******************************************************************
 *    ALTHOUGH THE __autoload() FUNCTION CAN ALSO BE USED FOR     *
 *     AUTOLOADING CLASSES AND INTERFACES, IT'S PREFERRED TO      *
 *   USE THE spl_autoload_register() FUNCTION. THIS IS BECAUSE    *
 *   IT IS A MORE FLEXIBLE ALTERNATIVE (ENABLING FOR ANY NUMBER   *
 *   OF AUTOLOADERS TO BE SPECIFIED IN THE APPLICATION, SUCH AS   *
 * IN THIRD PARTY LIBRARIES). FOR THIS REASON, USING _autoload() *
 *     IS DISCOURAGED AND IT MAY BE DEPRECATED IN THE FUTURE.     *
 ******************************************************************/

 require 'config/init.php';
 #visit 'config/init.php' file to understand autoload

    $myObj1= new myClass1();
    $myObj2= new myClass2();

 ?>