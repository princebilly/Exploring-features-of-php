<?php
//
// ────────────────────────────────────────────────────────────────────── I ──────────
//   :::::: W H Y   W E   A R E   H E R E : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────────────────────
//

/**********************************************************************************
 * IT IS A GOOD WAY TO ORGANIZE YOUR PROJECT LIKE THIS. IT SEEMS A LITTLE CREEPY  *
 * IF WE HAVE TO CALL AUTOLOAD FUNCTIONS FOR EVERY PHP FILE WE CREATED SO I HAVE  *
 * CREATED ANOTHER FILE AND CALL THE AUTOLOADER FUNCTION HERE SO WE DON'T HAVE TO *
 * TYPE ALL THIS CODE EVERY TIME WE MAKE ANOTHER PHP FILE. NOW ALL WE HAVE TO DO  *
 *                             require 'config/init';                             *
 *                                       or                                       *
 *                             include 'config/init';                             *
 *                            ON THE TOP OF EVERY PHP                             *
 **********************************************************************************/
    //*
    spl_autoload_register(function($className){
        require_once 'lib/'.$className.'.php';
        
    });
    //*/

/************************************************************
 * IT IS BETTER TO USE spl_autoload_register THAN _autoload *
 ************************************************************/
    /*
    function __autoload($class_name){
        require_once 'lib/'.$class_name.'.php';
    }
    //*/
?>