<?php
    include 'config/init.php';
/***********************************************************************
 * NAMESPACE FEATURE IS GENARALLY USED WHEN HAVE CLASS WITH SAME NAMES *
 ***********************************************************************/
    use namespace1\subnamespace as nm1_sub;
/***************************************
 * WE CAN ALSO USE ALIAS FOR NAMESPACE *
 ***************************************/
    $obj= new namespace1\sameName();
    $obj1= new namespace2\sameName();
    //$obj3= new namespace1\subnamespace\sameName();  we could use this but i am going to use a alias
    $obj3= new nm1_sub\sameName(); //using alias
    $obj();
    $obj1();
    $obj3();
?>