<?php
//
// ──────────────────────────────────────────────────── I ──────────
//   :::::: B A S I C S : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────────
//

//
// ─── UNSET VARRABLES ────────────────────────────────────────────────────────────
//

/***************************************************
 *  IF WE DECLARE A VARIABLE BUT IT IS UNSET IT    *
 * WILL BEHAVES AS UNDEFINED A VARIABLE AND RETURN *
 *                 FALSE AS A BOOL                 *
 ***************************************************/

    $a_unset_variable;
    echo $a_unset_variable.'<br>'; //error
    var_dump((bool)$a_unset_variable);    //error & return a false value
    echo '<br>';
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Output:                                                                                                                            //
    // Notice: Undefined variable: a_unset_variable in /home/princebilly/lampstack/apache2/htdocs/web/phpexplore/variables.php on line 16 //
    //                                                                                                                                    //
    //                                                                                                                                    //
    // Notice: Undefined variable: a_unset_variable in /home/princebilly/lampstack/apache2/htdocs/web/phpexplore/variables.php on line 17 //
    // bool(false)                                                                                                                        //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// ─── NULL VARIABLES ─────────────────────────────────────────────────────────────
//

/**********************************
 * IF WE SET A VARRAIBLE TO NULL  *
 * IT WILL RETURN FALSE AS A BOOL *
 **********************************/
    $set_null=null;
    var_dump((bool)$set_null);
    echo '<br>';
    //////////////////
    // Output:      //
    // bool(false)  //
    //////////////////
//
// ────────────────────────────────────────────────────────────────────────── II──────────
//   :::::: G L O B A L   V A R R I A B L E S : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────────────────────────
//
/*******************************************************************************************
 * GLOBAL VARIABLE CAN BE ALSO BE ACCESSED BY SUPER GLOBAL VARRIABLE $GLOBALS['VAR_NAME']; *
 ******************************************************************************************/
    $global_var=4;
    function global_var_print(){
        echo $GLOBALS['global_var'].'<br>';   //we don't need global keyword here
    }
    global_var_print();
    ////////////
    // Output://
    // 4      //
    ////////////

//
// ──────────────────────────────────────────────────────────────────────────── III─────────
//   :::::: V A R I A B L E   V A R I A B L E S : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────────────────────────────────
//

 /***************************************************************************************************************
 *    Code                     | This means this:                                                               *    
 *--------------------------------------------------------------------------------------------------------------*
 *   $variable_var= 'var_1';   | $variable_var='var_1'                                                          *
 *   $$variable_var= 'var_2';  | $$variable_var='var_2'     $var_1='var_2'                                      *
 *   $$$variable_var= 'var_3'; | $$$variable_var='var_3'    $$var_1='var_3'     $var_2='var_3'                  * 
 *   $$$$variable_var= 'value';| $$$$variable_var='value'   $$$var_1='value'    $$var_2='value' $var_3='value'  *
 ***************************************************************************************************************/

 //
 // ──────────────────────────────────────────────────────────────────────────────────────────────────────────────────── IV ──────────
 //   :::::: V A R R A I B L E   C A N   S T O R E   A L  M O S T   E V E R Y T H I N G : :  :   :    :     :        :          :
 // ──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
 //
//
// ─── FUNCTION VARRIABLE ─────────────────────────────────────────────────────────
//
/***********************************
 * PHP SUPPORTS FUNCTION VARRAIBLE *
 ***********************************/
    function dummyfunc1(){
        echo 'This function does nothing'.'<br>';
    }

    $func_var='dummyfunc1';
    $func_var(); //we can call like this //output: This function does nothing

#another Example:
/****************************************************
 * WE CAN ALSO CREATE ANONYMOUS FUNCTIONS LIKE THIS *
 ****************************************************/
    $func_var=function(){
        echo 'this is a unknown function'.'<br>';
    };

    $func_var();  // echo 'this is a unknown function';

// ────────────────────────────────────────────────────────────────────────────────
#calling function insde a class using function varraible
    class dummyClass{
        function class_func1(){
            echo 'class function normal'.'<br>';
        }
        static function class_func2(){
            echo 'class function static'.'<br>';
        }
    }
    $dummyObj = new dummyClass();   
/*************************************************************
 * TO CALL A FUNCTION INSIDE A CLASS USING FUNCTION VARIABLE *
 *                  WE HAVE TO USE AN ARRAY                  *
 *************************************************************/
    $func_var=array($dummyObj,"class_func1"); //calling normal function
    $func_var();
    $func_var=array("dummyClass","class_func2"); //calles static function
    $func_var();

//
// ─── CLASS VARIABLE ─────────────────────────────────────────────────────────────
//
/*********************************************
 * WE CAN STORE CLASS NAME INSIDE A VARIABLE *
 *********************************************/
    $class_var= 'dummyClass';
    $newObj= new $class_var();
    $newObj->class_func1();
    //////////////////////////
    // Output:              //
    // class function normal//
    //////////////////////////
//
// ──────────────────────────────────────────────────────────────────────────────────────────── V ──────────
//   :::::: N O N C A S E   S E N S I T I V E   C O N S T A N T : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────────────────────────────────────────────────
//

/**************************************************************
 * DEFINE ALSO ALLOWS US TO CREATE NON CASE SESITIVE CONSTANT *
 *                     HERE IS A EXAMPLE:                     *
 **************************************************************/
    define('Constant','Hello world',true); //define(const_name,const_vale,non-case-sensivity);
    echo CONSTANT.'<br>'.constant.'<br>'.CoNsTaNt.'<br>';

    ////////////////
    // Output:    //
    // Hello world//
    // Hello world//
    // Hello world//
    ////////////////
//
// ────────────────────────────────────────────────────────────────────── END ─────
//

        

?>