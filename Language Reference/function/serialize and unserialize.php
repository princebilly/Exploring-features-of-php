<?php
//
// ────────────────────────────────────────────────────────────────────────────────────────────────────────── I ──────────
//   :::::: W H A T   I S   S E R I A L I Z E   A N D   U N S E R I A L I Z E : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
//

/***********************************************************************
 * serialize() STORES THE VALUES OF A DATASTRUCTURE IN A STRING FORMAT *
 ***********************************************************************/
/***************************************************************************
 * unserialize() RESTORES VALUES FROM SERIALIZED DATASTRUCTURE WHICH IS IN *
 *                                 FORMAT                                  *
 ***************************************************************************/

 //
 // ────────────────────────────────────────────────────── II ──────────
 //   :::::: E X A M P L E : :  :   :    :     :        :          :
 // ────────────────────────────────────────────────────────────────
 //
    class foo{
    public $var;
    private $var_p;
        public function setVar($string1, $string2){
            $this->var= $string1;
            $this->var_p= $string2;
        }
    }

    $integer= 6;
    $string="Hello world";
    $array= array(
        'name'=>'Prince Billy',
        'Age'=>20,
        'Job'=>'Web devoloper'
    );
    $arraynum=array(10,9,12);
    $class_var=(new foo);
    $class_var->setVar('Something','Something untold');
 
    var_dump($integer); echo '<br>';
    var_dump($string); echo '<br>';
    var_dump($array); echo '<br>';
    var_dump($arraynum); echo '<br>';
    var_dump($class_var); echo '<br>';echo '<br>';

    $string1= serialize($integer);
    $string2= serialize($string);
    $string3= serialize($array);
    $string4= serialize($arraynum);
    $string5= serialize($class_var);

//
// ─── SERIALIZING: ───────────────────────────────────────────────────────────────
//    
    var_dump($string1); echo '<br>';
    var_dump($string2); echo '<br>';                //serialize store datastructre
    var_dump($string3); echo '<br>';                //data in a string
    var_dump($string4); echo '<br>';
    var_dump($string5); echo '<br>';echo '<br>';

    var_dump($integer); echo '<br>';                //Serialize doesn't impact the
    var_dump($string); echo '<br>';                 //main varriables
    var_dump($array); echo '<br>';
    var_dump($arraynum); echo '<br>';
    var_dump($class_var); echo '<br>';echo '<br>';  

//
// ─── UNSERIALIZING ──────────────────────────────────────────────────────────────
//        
    #unserializing:
    $newinteger= unserialize($string1);             //unserialize convert
    $newstring=  unserialize($string2);             //strings to datastructure
    $newarray=   unserialize($string3);
    $newarraynum=unserialize($string4);
    $newclass_var=unserialize($string5);

    var_dump($newinteger); echo '<br>';                
    var_dump($newstring); echo '<br>';                     
    var_dump($newarray); echo '<br>';
    var_dump($newarraynum); echo '<br>';
    var_dump($newclass_var); echo '<br>';echo '<br>';

/********************************************************************************************************
 * BY USING __sleep() and __wakeup() MAGIC FUNCTION WE CAN MODIFY THE SERILIZE AND UNSERIALIZE FUNCTION *
 *                                             FOR A CLASS                                              *
 ********************************************************************************************************/

//
// ────────────────────────────────────────────────────────────────────────────── III ──────────
//   :::::: W H A T   T H E Y   C A N N O T   D O : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────────────────────────────
//

/**************************************************************************
 * WE CANNOT SERIALIZE ANONYMOUS CLASS AND FUNCTION STORED IN A VARRAIBLE *
 **************************************************************************/

?>