<?php
//
// ──────────────────────────────────────────────────────────────────────────────────────────────────── I ──────────
//   :::::: V A R E X P O R T   V S   V A R D U M P   V S   P R I N T R : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────────────────────────────────────────────────────────
//

/****************************************************************************************
 * * VAR_EXPORT() — OUTPUTS OR RETURNS A PARSABLE STRING REPRESENTATION OF A VARIABLE * *
 ****************************************************************************************/
/****************************************************************************************
 * *                 VAR_DUMP() - DUMPS INFORMATION ABOUT A VARIABLE                  * *
 ****************************************************************************************/
/****************************************************************************************
 * *          PRINT_R() - PRINTS HUMAN-READABLE INFORMATION ABOUT A VARIABLE          * *
 ****************************************************************************************/
    
    $var= 45;
    echo 'Integer<br>';
    echo 'var_dump(): ';
    var_dump($var);
    echo '<br>';
    echo 'var_export(): ';
    var_export($var);
    echo '<br>';
    echo 'print_r(): ';
    print_r($var);
    echo '<br>';echo '<br>';

    ////////////////////////
    // Integer            //
    // var_dump(): int(45)//
    // var_export(): 45   //
    // print_r(): 45      //
    ////////////////////////

    $var= 34.54;
    echo 'float<br>';
    echo 'var_dump(): ';
    var_dump($var);
    echo '<br>';
    echo 'var_export(): ';
    var_export($var);
    echo '<br>';
    echo 'print_r(): ';
    print_r($var);
    echo '<br>';echo '<br>';
    //////////////////////////////
    // float                    //
    // var_dump(): float(34.54) //
    // var_export(): 34.54      //
    // print_r(): 34.54         //
    //////////////////////////////


    $var= array('Name'=> 'Prince', 'age'=>20, 'job'=>'webdevoloper');
    echo 'Array:<br>';
    echo 'var_dump(): ';
    var_dump($var);
    echo '<br>';
    echo 'var_export(): ';
    var_export($var);
    echo '<br>';
    echo 'print_r(): ';
    print_r($var);
    echo '<br>';echo '<br>';
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Array:                                                                                                       //
    // var_dump(): array(3) { ["Name"]=> string(6) "Prince" ["age"]=> int(20) ["job"]=> string(12) "webdevoloper" } //
    // var_export(): array ( 'Name' => 'Prince', 'age' => 20, 'job' => 'webdevoloper', )                            //
    // print_r(): Array ( [Name] => Prince [age] => 20 [job] => webdevoloper )                                      //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    $var='This is a string';
    echo 'string<br>';
    echo 'var_dump(): ';
    var_dump($var);
    echo '<br>';
    echo 'var_export(): ';
    var_export($var);
    echo '<br>';
    echo 'print_r(): ';
    print_r($var);
    echo '<br>';echo '<br>';
    //////////////////////////////////////////////
    // string                                   //
    // var_dump(): string(16) "This is a string"//
    // var_export(): 'This is a string'         //
    // print_r(): This is a string              //
    //////////////////////////////////////////////

    class myClass{
        public $var =100;
        private $var_p=0;
        public static $var_s = 200;
        private static $var_sp =100;
        public function demo(){
            echo 'This function does nothing<br>';
        }
    }
    $var= new myClass;
    echo 'Class<br>';
    echo 'var_dump(): ';
    var_dump($var);
    echo '<br>';
    echo 'var_export(): ';
    var_export($var);
    echo '<br>';
    echo 'print_r(): ';
    print_r($var);
    echo '<br>';echo '<br>';
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    //    Class                                                                                       //
    //    var_dump(): object(myClass)#1 (2) { ["var"]=> int(100) ["var_p":"myClass":private]=> NULL } //
    //    var_export(): myClass::__set_state(array( 'var' => 100, 'var_p' => NULL, ))                 //
    //    print_r(): myClass Object ( [var] => 100 [var_p:myClass:private] => )                       //
    ////////////////////////////////////////////////////////////////////////////////////////////////////
 ?>

 