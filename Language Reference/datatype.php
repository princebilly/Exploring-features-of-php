<?php
//
// ────────────────────────────────────────────────────── I ──────────
//   :::::: I N T E G E R : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────
//

/***********************************************************************
 * WE CAN ALSO ASIGN OCTAL, HEXADECIMAL AND BINARY IN INTEGER DATATYPE *
 ***********************************************************************/
    $intval=0123;                                //This is a octal number
    echo 'Octal(123): '.$intval.'<br>';          //This will be printed in decimal
    $intval=0x123;                               //This is a hexadecimal number
    echo 'Hexadecimal(123): '.$intval.'<br>';    //This will be printed in decimal
    $intval=0b111.'<br>';                        //This is binary number
    echo 'Binary(111): '.$intval;                //This will be printed in decimal

    //////////////////////////
    // Octal(123): 83       //
    // Hexadecimal(123): 291//
    // Binary(111): 7       //
    //////////////////////////
        
//
// ──────────────────────────────────────────────────── II ──────────
//   :::::: S T R I N G S: :  :   :    :     :        :         :
// ──────────────────────────────────────────────────────────────
//

//
// ─── SINGELE QOUTED ─────────────────────────────────────────────────────────────
//

/***************************************************************************
 * IT IS OK TO DO NEW LINES IN STRING WHILE WRITING CODE BUT THE NEW LINES *
 *                        WILL NOT PRINTED IN HTML                         *
 ***************************************************************************/

    echo 'Lets \'test\' does new 
    line here will also work 
    ther or not<br>';

    ////////////////////////////////////////////////////////////
    // Lets test does new line here will also work ther or not//
    ////////////////////////////////////////////////////////////

//
// ─── DOUBLE QOUTED ──────────────────────────────────────────────────────────────
//`

/*************************************************************************************
 * NONE OF THE DOUBLE QOUTE OR SINGLE QOUTED STRING SUPPORTS BLACK SLASH CHARRECTORS *
 *************************************************************************************/
    echo 'new\nline'.'<br>'; //Outputs: new\nline
    print 'new\nline'.'<br>'; //Outputs: new\nline
    #double qoute will remove the charrector constant while printed
    echo "new\nline".'<br>'; //Outputs: new line 
    print "new\nline".'<br>';//Outputs: new line 
     
//
// ─── HEREDOC ────────────────────────────────────────────────────────────────────
//

/*******************************************************************************************************
 * HEREDOC CAN BE USE TO STORE MULTIPLE LINE IN  A VARRAIBLE BEST WORKS FOR SQL QUERY LANGUAGE STORING *
 *******************************************************************************************************/
    $sql=<<<'SQL'
    CREATE TABLE Persons (                 
        ID int NOT NULL,                   
        LastName varchar(255) NOT NULL,    
        FirstName varchar(255),           
        Age int,                           
        UNIQUE (ID)                        
    );                                     
SQL;
/********************************************************************************
 * A HEREDOC CANNOT BE INTENDED. WE ALSO CANNOT PUT ANY COMMENT IN ANY OF THOSE *
 *              LINE THAT CONTAINS HEREDOC TEXTS OR VARRAIBLE NAME              *
 ********************************************************************************/
    echo $sql.'<br>';
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Output:                                                                                                                //
    // CREATE TABLE Persons ( ID int NOT NULL, LastName varchar(255) NOT NULL, FirstName varchar(255), Age int, UNIQUE (ID) );//
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*****************************************************************
 * ALL NEW LINES WE HAVE ADDED IN A HERDOC ARE NOT BE CONSIDERED *
 *****************************************************************/

    

//
// ────────────────────────────────────────────────── III ──────────
//   :::::: A R R A Y : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────
//
//
// ─── REMOVE ARRAY  AND ARRAY ELEMENT ──────────────────────────────────────
//
/*************************************************************************
 * TO REMOVE A ARRAY ELEMENT WE CAN USE unset($VAR) / unset($VAR[INDEX]) *
 *************************************************************************/
    $array= array('1','2','3');
    print_r($array);
    echo '<br>';
    unset($array[2]);
    print_r($array);
    echo '<br>';
    unset($array);
    print_r($array);   //Here we will get a error
    echo '<br>';
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //     Output:                                                                                                                //
    //     Array ( [0] => 1 [1] => 2 [2] => 3 )                                                                                   //
    //     Array ( [0] => 1 [1] => 2 )                                                                                            //
    //                                                                                                                            //
    //     Notice: Undefined variable: array in /home/princebilly/lampstack/apache2/htdocs/web/phpexplore/datatype.php on line 106//
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// ─── CHECK IF A ARRAY KEY EXISTS ────────────────────────────────────────────────
//

    $array= array('1','2','3');
    if(array_key_exists(2,$array))
        echo 'Array key exisis<br>';
    else                               //exists
        echo 'Array key not exists<br>';

    if(array_key_exists(3,$array))
        echo 'Array key exisis<br>';
    else                                //Not exists
        echo 'Array key not exists<br>';

/*********************************************************************************
 * TO CHECK IF A ARRAY KEY EXISTS OR NOT, WE CAN USE array_key_exists() FUNCTION *
 *********************************************************************************/

//
// ──────────────────────────────────────────────────────── IV ──────────
//   :::::: C A L L A B L E : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────────────
//
/***********************************************************
 * CALLABLE DATATYPES ARE USED AS ARGUMENTS FOR A FUNCTION *
 *        (PASSING ANOTHER FUNCTIONS IN A FUNCTION)        *
 ***********************************************************/
    function foo($var){
        if(is_callable($var)){
            call_user_func($var);
        }else{
            echo $var.'<br>';
        }
    }
    function boo(){
        echo 'boo() is called'.'<br>';
    }
    $foovar='Hello world';
    foo($foovar);         //calling with a varriable
    foo(boo());          //calling with another function
    foo(function(){     //calling with another function
        echo 'foo() called a function()'.'<br>';
        
    })

//
// ────────────────────────────────────────────────────────────────────── END ─────
//
?>
