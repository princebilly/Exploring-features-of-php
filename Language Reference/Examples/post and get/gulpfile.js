var gulp= require('gulp');
var sass= require('gulp-sass');
var autoprefixer= require('gulp-autoprefixer');
var notify= require('gulp-notify');

/*=====================================
=                config              =
======================================*/
//var autoprefixBrowsers = ['last 2 versions'];
var autoprefixBrowsers = ['> 1%', 'last 2 versions', 'firefox >= 4', 'safari 7', 'safari 8', 'IE 8', 'IE 9', 'IE 10', 'IE 11'];
/*=====================================
=                 tasks               =
======================================*/


gulp.task('message', function() {
    return  console.log('gulp is running');
});

gulp.task('copyphp', function() {
    gulp.src('src/**/*.php')
      .pipe(gulp.dest('dist'));
});

gulp.task('sass', function() {
    gulp.src('src/sass/style.scss')
    .pipe(sass({outputStyle: 'expanded'}).on('error',function(err) {
            sass.logError;
            return notify().write(err);
        }))
    .pipe(autoprefixer({browsers:autoprefixBrowsers}))
    .pipe(gulp.dest('dist/css'))
    .pipe(notify({message: 'sass compiled successfully'}));
});

/*=====================================
=              core tasks             =
======================================*/

gulp.task('default',['message','sass','copyphp']);

gulp.task('watch',function() {
    gulp.watch('src/sass/**/*.scss', ['sass']);
    gulp.watch('src/**/*.php', ['copyphp']);
});

